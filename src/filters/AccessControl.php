<?php

namespace myzero1\authz\filters;
use Yii;
use yii\base\Action;
use yii\base\ActionFilter;
use yii\di\Instance;
use yii\web\ForbiddenHttpException;
use yii\web\User;

/**
 * AccessControl provides simple access control based on a set of config.
 *
 * @author myzero1 <myzero1@qq.com>
 * @since 1.0.0
 */
class AccessControl extends ActionFilter
{
    /**
     * @var User|array|string|false the user object representing the authentication status or the ID of the user application component.
     * Starting from version 2.0.2, this can also be a configuration array for creating the object.
     * Starting from version 2.0.12, you can set it to `false` to explicitly switch this component support off for the filter.
     */
    public $user = 'user';

    /**
     * @var string role table name
     */
    public $roleTableName = 'z1role';

    /**
     * @var string user table name
     */
    public $userTableName = 'user';
    
    /**
     * @var string control type
     * everyone login   rbac
     */
    public $controlType = 'rbac';
    /**
     * @var string role id field name in user table
     */
    public $roleIdFieldName = 'role_id'; 
    /**
     * @var string super admin id
     */
    public $superadminId = '1';
    /**
     * @var array exclude uri
     */
    public $excludeUri = [
        '/site/login:*',
        '/site/error:GET',
    ];
    /**
     * @var array logined exclude uri
     */
    public $loginedExcludeUri = [
        '/site/logout:*',
        '/site/index:*',
    ];
    /**
     * @var array super admin uri
     */
    public $superadminUri = [
        '/rbacp/default/migrate-up:*',
        '/rbacp/default/migrate-down:*',
    ];

    /**
     * @var callable|array a callback that will be called if the access should be denied
     * to the current user. 
     */
    public $permissionCallback=[
        '1' => [
            'pId' => '1',
            'name' => '全部',
            'value' => '',
        ],

        '11' => [
            'pId' => '1',
            'name' => '用户管理',
            'value' => '',
        ],
        '1101' => [
            'pId' => '11',
            'name' => '用户管理-列表',
            'value' => '/authz/user/index:GET',
        ],
        '1102' => [
            'pId' => '11',
            'name' => '用户管理-创建',
            'value' => '/authz/user/create:*',
        ],
        '1103' => [
            'pId' => '11',
            'name' => '用户管理-修改',
            'value' => '/authz/user/update:*',
        ],
        '1104' => [
            'pId' => '11',
            'name' => '用户管理-详情',
            'value' => '/authz/user/view:*',
        ],
        '1105' => [
            'pId' => '11',
            'name' => '用户管理-删除',
            'value' => '/authz/user/delete:*',
        ],

        '12' => [
            'pId' => '1',
            'name' => '角色管理',
            'value' => '',
        ],
        '1201' => [
            'pId' => '12',
            'name' => '角色管理-列表',
            'value' => '/authz/z1role/index:GET',
        ],
        '1202' => [
            'pId' => '12',
            'name' => '角色管理-创建',
            'value' => '/authz/z1role/create:*',
        ],
        '1203' => [
            'pId' => '12',
            'name' => '角色管理-修改',
            'value' => '/authz/z1role/update:*',
        ],
        '1204' => [
            'pId' => '12',
            'name' => '角色管理-详情',
            'value' => '/authz/z1role/view:*',
        ],
        '1205' => [
            'pId' => '12',
            'name' => '角色管理-删除',
            'value' => '/authz/z1role/delete:*',
        ],
    ];

    /**
     * @var callable|null a callback that will be called if the access should be denied
     * to the current user. 
     */
    public $denyCallback;

    /**
     * Initializes the module from configurations.
     */
    public function init()
    {
        parent::init();
        if ($this->user !== false) {
            $this->user = Instance::ensure($this->user, User::className());
        }
    }

    /**
     * This method is invoked right before an action is to be executed (after all possible filters.)
     * You may override this method to do last-minute preparation for the action.
     * @param Action $action the action to be executed.
     * @return bool whether the action should continue to be executed.
     */
    public function beforeAction($action)
    {
        if ('db') {
            $db = \Yii::$app->db;

            if (is_null($db->getTableSchema($this->roleTableName,true))) {
                try {
                    $sql=sprintf(
                        '
                            CREATE TABLE `%s` (
                                `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                                `des` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                                `permissions` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                                `status` smallint(6) NOT NULL DEFAULT 2, -- 1enabled,2disabled
                                `created_at` bigint(20) NOT NULL,
                                `updated_at` bigint(20) NOT NULL,
                                `deleted_at` bigint(20) NOT NULL DEFAULT 0,
                                PRIMARY KEY (`id`),
                                UNIQUE KEY `name` (`name`)
                            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
                        ',
                        $this->roleTableName
                    );
                    $res = $db->createCommand($sql)->execute();
                    $db->getTableSchema($this->roleTableName,true);
                } catch (\Throwable $th) {
                    throw $th;
                }

                if (is_null($db->getTableSchema($this->userTableName,true))) {
                    try {
                        $sql=sprintf(
                            '
                                CREATE TABLE `%s` (
                                    `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                    `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                                    `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
                                    `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                                    `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                                    `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                                    `status` smallint(6) NOT NULL DEFAULT 10,
                                    `created_at` bigint(20) NOT NULL,
                                    `updated_at` bigint(20) NOT NULL,
                                    `role_id` bigint(20) NOT NULL DEFAULT 0,
                                    `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                                    PRIMARY KEY (`id`),
                                    UNIQUE KEY `username` (`username`),
                                    UNIQUE KEY `email` (`email`),
                                    UNIQUE KEY `password_reset_token` (`password_reset_token`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
                            ',
                            $this->userTableName
                        );
                        $res = $db->createCommand($sql)->execute();
                        $db->getTableSchema($this->userTableName,true);
        
                        $hash = Yii::$app->getSecurity()->generatePasswordHash('admin@735');
                        $user=new \Yii::$app->user->identityClass();
                        $user->username='superadmin';
                        $user->password_hash=$hash;
                        $user->status=10;
                        $user->auth_key='auth_key';
                        $user->email='superadmin@myzero1.com';
                        $user->id=1;
                        $user->save();
                    } catch (\Throwable $th) {
                        throw $th;
                    }
                }
            }
        }

        if ('check') {
            // $this->z1debug($this->superadminId);
            // $this->z1debug($this->user);
            // $this->z1debug(Yii::$app->getRequest()->url.':'.Yii::$app->getRequest()->method);
            $result=false;

            if ($this->controlType=='everyone') {
                $result=true;
                // $this->z1debug('1result:'.$result);
            } else if ($this->controlType=='login') {
                // $this->z1debug('2result:'.$result);
                // var_dump();
                // var_dump($this->superadminId,\Yii::$app->user->id);exit;

                if ($this->user->getIsGuest()) {
                    $result=$this->checkExcludeUri($this->excludeUri);
                }else{
                    if ($this->superadminId==\Yii::$app->user->id) {
                        $result=true;
                    } else {
                        $result=$this->checkExcludeUri($this->loginedExcludeUri);
                    }
                }
            } else if ($this->controlType=='rbac') {
                // $this->z1debug('3result:'.$result);
                if ($this->user->getIsGuest()) {
                    $result=$this->checkExcludeUri($this->excludeUri);
                }else{
                    if ($this->superadminId==\Yii::$app->user->id) {
                        $result=true;
                    } else {
                        $result=$this->checkExcludeUri($this->loginedExcludeUri);
                        if ($result) {
                            $result=true;
                        } else {
                            if ('rbac') {
                                $roleIdFieldName=$this->roleIdFieldName;
                                $query = (new yii\db\Query())
                                ->from($this->roleTableName)
                                ->select([
                                    'permissions',
                                ])
                                ->andFilterWhere([
                                    'and',
                                    ['=', 'id', Yii::$app->user->identity->$roleIdFieldName],
                                ]);
                                $role=$query->one();
            
                                if (!$role) {
                                    $result=false;
                                } else {
                                    $permissions=self::getPermission();

                                    $permissionsTmp=[];
                                    $per=explode(',',$role['permissions']);
                                    $per=array_filter($per);
                                    foreach ($per as $k => $v) {
                                        $permissionsTmp[]=$permissions[$v]['value'];
                                    }

                                    $result=$this->checkExcludeUri($permissionsTmp);
                                }
                            }
                        }
                    }
                }
            } else {
                // $this->z1debug('4result:'.$result);
                $result=false;
            }
            // $this->z1debug('result:'.$result);
            return $this->resultFunction(
                $result,
                $action
            );
        }
    }

    /**
     * Denies the access of the user.
     * The default implementation will redirect the user to the login page if he is a guest;
     * if the user is already logged, a 403 HTTP exception will be thrown.
     * @param User|false $user the current user or boolean `false` in case of detached User component
     * @throws ForbiddenHttpException if the user is already logged in or in case of detached User component.
     */
    protected function denyAccess($user)
    {
        if ($user !== false && $user->getIsGuest()) {
            $user->loginRequired();
        } else {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    protected function checkExcludeUri($excludeUri)
    {
        $uri=\yii\helpers\Url::toRoute([Yii::$app->controller->action->id]);
        $srcAct=sprintf('%s:%s',$uri,Yii::$app->request->method);
        $srcAct2=sprintf('%s:*',$uri);

        // var_dump($srcAct,$srcAct2,$excludeUri);exit;

        if (in_array($srcAct,$excludeUri)) {
            return true;
        } else if (in_array($srcAct2,$excludeUri)) {
            return true;
        } else {
            return false;
        }
    }

    protected function resultFunction($result,$action)
    {
        $user=Yii::$app->user;
        if ($result) {
            return $result;
        } else {
            if ($this->denyCallback !== null) {
                call_user_func($this->denyCallback, null, $action);
            } else {
                $this->denyAccess($user);
            }

            return false;
        }
    }

    protected function z1debug($msg)
    {
        $logFile=Yii::getAlias('@runtime/z1debug.log');
        $msg=date('Y-m-d H:i:s').json_encode($msg)."\n";
        file_put_contents($logFile,$msg,FILE_APPEND );
    }

    public function getPermission()
    {
        if (is_array($this->permissionCallback)) {
            $permissions=$this->permissionCallback;
        } else {
            // $permissions=$this->permissionCallback();
            call_user_func($this->permissionCallback, null);
        }

        return $permissions;
    }

    public function ztreePermission()
    {
        $permissions=self::getPermission();
        $nodes=[];
        foreach ($permissions as $k => $v) {
            $v['id']=$k;
            $nodes[]=$v;
        }

        return $nodes;
    }
}
