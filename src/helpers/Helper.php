<?php

/**
 * @link https://gitee.com/myzero1/yii2-authz
 * @copyright Copyright (c) 2019-7357 myzero1
 * @license Apache2
 */

namespace myzero1\authz\helpers;

/**
 * Class Helper provides some useful static methods.
 *
 * For all user.
 *
 * @author Myzero1 <myzero1@qq.com>
 * @since 0.0.1
 */
class Helper
{
    /**
     * @param string $groupKey status
     * @param string $key 1
     * @param array $customDict ['1'=>'启用','2'=>'禁用',]
     * @param mixed $noLable false  '' 'not found'
     * @return string
     */
    public static function getDictLable($groupKey,$key,$customDict=false,$noLable=false)
    {
        $result=$key;

        if ($customDict!==false) {
            if (isset($customDict[$key])) {
                $result=$customDict[$key];
            } else {
                if ($noLable!==false) {
                    $result=$noLable;
                }
            }
        } else{
            if (isset(\Yii::$app->params['dict']) && isset(\Yii::$app->params['dict'][$groupKey]) && isset(\Yii::$app->params['dict'][$groupKey][$key]) ) {
                    $result=\Yii::$app->params['dict'][$groupKey][$key];
            } else{
                if ($noLable!==false) {
                    $result=$noLable;
                }
            }
        }

        return $result;
    }

    /**
     * @param int $timestamp 1552886962 the will be convered timestamp.
     * @return string 2019-03-23 19:10:23
     */
    public static function time2string($timestamp,$format='Y-m-d H:i:s')
    {
        if (empty($timestamp)) {
            return '';
        } else {
            if (self::isTimestamp($timestamp)) {
                return date($format, intval($timestamp));
            } else {
                return '';
            }
        }
    }

    /**
     * @param string $string 2019-03-23 19:10:23
     * @return int 1552886962
     */
    public static function string2time($string)
    {
        if (empty($string)) {
            return 0;
        } else {
            return strtotime($string);
        }
    }

    /**
     * @param mixed $timestamp .
     * @return bool
     */
    public static function isTimestamp($timestamp)
    {
        $timestamp = intval($timestamp);
        if (strtotime(date('Y-m-d H:i:s', $timestamp)) === $timestamp) {
            return true;
        } else {
            return false;
        }
    }

}