<?php

namespace myzero1\authz\authz\controllers;

use yii\web\Controller;

/**
 * Default controller for the `authz` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
