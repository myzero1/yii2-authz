<?php

namespace myzero1\authz\authz;

/**
 * authz module definition class
 */
class Authz extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'myzero1\authz\authz\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
        \Yii::$app->params['authzInstance']=new \myzero1\authz\filters\AccessControl();
    }
}
