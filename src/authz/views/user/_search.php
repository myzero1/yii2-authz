<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var myzero1\authz\authz\models\search\Z1roleSearcher $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="user-search search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'username')->label('')->textInput(['placeholder'=>'username']) ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<style>
    .search{
        overflow: hidden;
    }
    .form-group{
        float: left;
        margin-right: 10px;
    }
    .form-group label{
        display: none;
    }
</style>