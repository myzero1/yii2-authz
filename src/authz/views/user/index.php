<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var myzero1\authz\authz\models\search\Z1roleSearcher $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <?= Html::a('Go Users', ['user/index'], ['class' => 'btn btn-success']) ?>
    &nbsp;&nbsp;
    <?= Html::a('Go Z1role', ['z1role/index'], ['class' => 'btn btn-success']) ?>

    <h1><?= Html::encode($this->title) ?></h1>
    <br/>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            'role_id'=>[
                'label' => 'role',
                'attribute' => \Yii::$app->params['authzInstance']->roleIdFieldName,
                'value' => function($row){
                    $role=\myzero1\authz\authz\models\Z1role::find()->where(['id'=>$row['role_id']])->one();
                    return $role['name'];
                }
            ],
            'created_at'=>[
                'label' => 'created_at',
                'attribute' => 'created_at',
                'value' => function($row){
                    return \myzero1\authz\helpers\Helper::time2string($row['created_at']);
                }
            ],
            'updated_at'=>[
                'label' => 'updated_at',
                'attribute' => 'updated_at',
                'value' => function($row){
                    return \myzero1\authz\helpers\Helper::time2string($row['updated_at']);
                }
            ],
            // 'auth_key',
            // 'password_hash',
            // 'password_reset_token',
            //'email:email',
            //'status',
            //'created_at',
            //'updated_at',
            //'verification_token',
            [
                'header' => 'operations',
                'class' => yii\grid\ActionColumn::className(),
                'template' => '{view}&nbsp;&nbsp;&nbsp;&nbsp;{update}&nbsp;&nbsp;&nbsp;&nbsp;{delete}',
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
