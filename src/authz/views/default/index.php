<div class="authz-default-index">
    <h1><?= $this->context->action->uniqueId ?></h1>
    <p>
        This is the view content for action "<?= $this->context->action->id ?>".
        The action belongs to the controller "<?= get_class($this->context) ?>"
        in the "<?= $this->context->module->id ?>" module.
    </p>
    <p>
        You may customize this page by editing the following file:<br>
        <code><?= __FILE__ ?></code>
    </p>

    <br/>

    <?= yii\helpers\Html::a('Go Users', ['user/index'], ['class' => 'btn btn-success']) ?>
    &nbsp;&nbsp;
    <?= yii\helpers\Html::a('Go Z1role', ['z1role/index'], ['class' => 'btn btn-success']) ?>
</div>
