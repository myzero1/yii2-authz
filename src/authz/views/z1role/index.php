<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel myzero1\authz\authz\models\search\Z1roleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Z1roles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="z1role-index">

    <?= Html::a('Go Users', ['user/index'], ['class' => 'btn btn-success']) ?>
    &nbsp;&nbsp;
    <?= Html::a('Go Z1role', ['z1role/index'], ['class' => 'btn btn-success']) ?>

    <h1><?= Html::encode($this->title) ?></h1>
    <br/>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'des',
            // 'permissions:ntext',
            // 'status',
            //'created_at',
            //'updated_at',
            //'deleted_at',

            'created_at'=>[
                'label' => 'created_at',
                'attribute' => 'created_at',
                'value' => function($row){
                    return \myzero1\authz\helpers\Helper::time2string($row['created_at']);
                }
            ],
            'updated_at'=>[
                'label' => 'updated_at',
                'attribute' => 'updated_at',
                'value' => function($row){
                    return \myzero1\authz\helpers\Helper::time2string($row['updated_at']);
                }
            ],
            [
                'header' => 'operations',
                'class' => yii\grid\ActionColumn::className(),
                // 'template' => '{view} {update} {delete}',
                'template' => '{view}&nbsp;&nbsp;&nbsp;&nbsp;{update}&nbsp;&nbsp;&nbsp;&nbsp;{delete}',
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
