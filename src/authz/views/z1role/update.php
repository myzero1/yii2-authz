<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model myzero1\authz\authz\models\Z1role */

$this->title = 'Update Z1role: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Z1roles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="z1role-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
