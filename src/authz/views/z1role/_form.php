<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model myzero1\authz\authz\models\Z1role */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="z1role-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'des')->textInput(['maxlength' => true]) ?>

    <!-- <?= $form->field($model, 'permissions')->textarea(['rows' => 6]) ?> -->
    <?php
        $accessControl=new myzero1\authz\filters\AccessControl();
        $nodes=$accessControl->ztreePermission();
        $permissions=explode(',',$model->permissions);

        foreach ($nodes as $k => $v) {
            if (in_array($v['id'],$permissions)) {
                $nodes[$k]['checked']=true;
            }
        }

        echo $form->field($model, 'permissions')->hiddenInput()->widget(
            \mallka\ztree\Ztree::class,
            [
                'data'=>$nodes,
                'checkbox'=>true,
            ]
        );

    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<style>
    #z1role-permissions-ztree{
        border: solid 1px #ccc;
        border-radius: 4px;
        padding: 10px;
    }
</style>
