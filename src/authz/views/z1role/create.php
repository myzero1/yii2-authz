<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model myzero1\authz\authz\models\Z1role */

$this->title = 'Create Z1role';
$this->params['breadcrumbs'][] = ['label' => 'Z1roles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="z1role-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
