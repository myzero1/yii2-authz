<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model myzero1\authz\authz\models\Z1role */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Z1roles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="z1role-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'des',
            'permissions:ntext',
            'status',
            'created_at'=>[
                'label' => 'created_at',
                'attribute' => 'created_at',
                'value' => function($row){
                    return \myzero1\authz\helpers\Helper::time2string($row['created_at']);
                }
            ],
            'updated_at'=>[
                'label' => 'updated_at',
                'attribute' => 'updated_at',
                'value' => function($row){
                    return \myzero1\authz\helpers\Helper::time2string($row['updated_at']);
                }
            ],
            'deleted_at'=>[
                'label' => 'deleted_at',
                'attribute' => 'deleted_at',
                'value' => function($row){
                    return \myzero1\authz\helpers\Helper::time2string($row['deleted_at']);
                }
            ],
        ],
    ]) ?>

</div>
