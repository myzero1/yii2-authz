<?php

namespace myzero1\authz\authz\models;

use Yii;

/**
 * This is the model class for table "z1role".
 *
 * @property int $id
 * @property string $name
 * @property string $des
 * @property string $permissions
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 */
class Z1role extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'z1role';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'des', 'permissions', 'created_at', 'updated_at'], 'required'],
            [['permissions'], 'string'],
            [['status', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['name', 'des'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'des' => 'Des',
            'permissions' => 'Permissions',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
        ];
    }

    /**
     * {@inheritdoc}
     */
    static public function roleList()
    {
        $result=self::find()->select(['id','name'])->where(['and',['=','deleted_at',0]])->all();
        $roles=array_combine(array_column($result,'id'),array_column($result,'name'));

        return $roles;
    }
}
